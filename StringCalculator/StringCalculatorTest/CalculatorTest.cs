﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator.Calculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StringCalculatorTest
{
    [TestClass]
    public class CalculatorTest
    {
        private Calculator calculator;
        [TestInitialize]
        public void init()
        {
            calculator = new Calculator();
        }

        [TestMethod]
        public void emptyString()
        {
            Assert.AreEqual(0, calculator.add(""));
        }

        [TestMethod]
        public void singleValue()
        {
            Assert.AreEqual(5, calculator.add("5"));
        }
        [TestMethod]
        public void sumTwoValues()
        {
            Assert.AreEqual(11, calculator.add("5,6"));
        }

        [TestMethod]
        public void sumThreeValues()
        {
            Assert.AreEqual(7, calculator.add("2,3,2"));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void valueGreaterThanMax()
        {
            Assert.AreEqual(1001, calculator.add("1001,1002"));
        }

        [TestMethod]
        public void userDefinedDelimiterOnFirstLine()
        {
            Assert.AreEqual(11, calculator.add("A5A6"));
        }

        [TestMethod]
        public void userDefinedDelimiterOnMultiLines()
        {
            Assert.AreEqual(11, calculator.add("AXYZ5AXYZ6"));
        }

    }
}
