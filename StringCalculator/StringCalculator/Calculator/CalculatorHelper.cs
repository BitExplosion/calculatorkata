﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator
{
    public class CalculatorHelper
    {
        private bool isParsedOperandInRange(int parsedOperand)
        {
            return parsedOperand < 0 || parsedOperand > 1000;
        }

        public int parseOperandFromString(string operandString)
        {
            int parsedOperand = Int32.Parse(operandString);
            if (isParsedOperandInRange(parsedOperand))
            {
                throw new Exception("Out Or Range Exception");
            }
            return parsedOperand;
        }

    }
}
