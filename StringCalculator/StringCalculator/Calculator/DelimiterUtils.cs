﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator.Calculator
{
    class DelimiterUtils
    {
        public string defaultDelimiter = ",";
         private bool seekDelimiter(int seek,string equation)
        {
            int parseResult = 0;
            return !int.TryParse(equation[seek].ToString(), out parseResult) && seek < equation.Length;
        }

        public string extractDelimiter(string equation)
        {
            string userDelimiter = "";
            if (equation.Length >= 1)
            {
                for(int delimiterSeek=0; seekDelimiter(delimiterSeek,equation);delimiterSeek++)
                {
                    userDelimiter += equation[delimiterSeek].ToString();
                }

            }
            if(userDelimiter.Length>=1)
            {
                return userDelimiter;
            }
            return defaultDelimiter;
        }
    }
}
