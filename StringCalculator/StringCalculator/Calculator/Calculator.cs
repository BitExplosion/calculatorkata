﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator.Calculator
{
    public class Calculator
    {
     
        public int add(string equation)
        {
            int sum = 0;
            CalculatorHelper helper = new CalculatorHelper();
            DelimiterUtils delimiterUtility = new DelimiterUtils();
            string[] delimiters = new string[1];

            delimiters[0] = delimiterUtility.extractDelimiter(equation);
            string[] operands = equation.Split(delimiters,StringSplitOptions.RemoveEmptyEntries);
            for (int operandCount = 0; operandCount < operands.Length; operandCount++)
            {
                sum += helper.parseOperandFromString(operands[operandCount]);

            }
            return sum;
           
        }

      

      

       


    }
}
